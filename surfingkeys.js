// an example to create a new mapping `ctrl-y`
// mapkey('<Ctrl-y>', 'Show me the money', function() {
    // Front.showPopup('a well-known phrase uttered by characters in the 1996 film Jerry Maguire (Escape to close).');
// });

// an example to remove mapkey `Ctrl-i`
// unmap('<Ctrl-i>');


// Show usage: ? is better
map('?', 'u');
unmap('u')

// Copy settings - "Yank RC file"
map('yrc', 'yj');
unmap('vj');

// Go to settings - "Go to RC file"
map('grc', 'se');
unmap('se');

// Reset settings - "Undo changes to RC file"
map('urc', 'sr');
unmap('sr');

// History Back/Forward    (note: uppercase)
map('<Ctrl-h>','S');   // override native goto History (use gh instead)
map('<Ctrl-l>','D');
unmap('S');
unmap('D');

// Tab History Back/Forward
// Previous/Next Tab
map('H','E');
map('L','R');
unmap('E');
unmap('R');

// Tab History Backward/Forward
map('<Ctrl-i>','F');
map('<Ctrl-o>','B');  // overrides native open...like anyone uses that
unmap('B');
unmap('F');
unmap('af');
map('A','cf');    //Open multiple links in a new tab
map('a','gf');    //Open a link in non-active new tab
//map('F','af');    //Open a link in new tab
mapkey('F', '#1Open a link in new tab', 'Hints.create("", Hints.dispatchMouseClick, {tabbed: true})');
unmap('cf')
unmap('gf')
//unmap('af')

// Scroll Page Down/Up
map('J','d');
map('K','e');
unmap('d');
unmap('e');


// The native shortcuts for chrome are fine
unmap('r');     // reload is ctrl+r
unmap('zi');     // zoom-in is ctrl+plus
unmap('zo');     // zoom-out is ctrl+minus
unmap('zr');     // zoom-reset is ctrl+0
unmap('ga');     // help page

// Not sure how (if?) these functions work, so remove for now
    // goto First + Last Tab 
    unmap('g0');
    unmap('g$');
    // show key pressed/last function
    unmap('<Ctrl-1>');
    unmap('<Ctrl-2>');
    



// click `Save` button to make above settings to take effect.
// set theme
settings.theme = '\
.sk_theme { \
    background: #fff; \
    color: #000; \
} \
.sk_theme tbody { \
    color: #000; \
} \
.sk_theme input { \
    color: #000; \
} \
.sk_theme .url { \
    color: #555; \
} \
.sk_theme .annotation { \
    color: #555; \
} \
.sk_theme .focused { \
    background: #f0f0f0; \
}';
settings.smoothScroll = false;
